import cv2
from time import sleep
from datetime import datetime

class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        #self.video = cv2.VideoCapture(0)
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        self.video = cv2.VideoCapture('video.mp4')
        self.fps = self.video.get(cv2.cv.CV_CAP_PROP_FPS) # Gets the frames per second - See more at: http://www.mzan.com/article/22704936-reading-every-nth-frame-from-videocapture-in-opencv.shtml#sthash.aB9EJwbf.dpuf
        self.frames_to_skip = int(self.fps/6)
        print("Video FPS: " + str(self.fps) + ", frames to skip: " + str(self.frames_to_skip))
        self.frames_read = 0
        self.frames_sent = 0
    
    def __del__(self):
        self.video.release()
    
    def get_frame(self,app, client_info):
        #success, image = self.video.read()

        # read only 4 images per seconds, skip the rest
        # rather nasty way, but just skip what we do not want
        for i in range(self.frames_to_skip): 
            self.frames_read = self.frames_read + 1
            success, image = self.video.read()
            if success == False:
                self.video = cv2.VideoCapture('video.mp4')        
                success, image = self.video.read()
                app.logger.info('restarting stream for client: ' + client_info )

        # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # so we must encode it into JPEG in order to correctly display the
        # video stream.

        # put timestamp into the image
        time_to_display = str(datetime.now())
        cv2.putText(image, time_to_display, (5, 25), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255,255,255), 4, 4)
        cv2.putText(image, time_to_display, (7, 25), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,0,0), 2, 4)

        ret, jpeg = cv2.imencode('.jpg', image)
        #sleep(0.23)
        sleep(0.10)
        self.frames_sent = self.frames_sent + 1
        return jpeg.tobytes()