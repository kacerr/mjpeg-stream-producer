### BUILD
docker build -t mjpeg-stream-faker .

### How to run
```
docker run -p 5000:5000 --name=mjpeg-stream-faker mjpeg-stream-faker /srv/run.sh
```

### specific run in dev env
```
docker run -p 5000:5000 -e VIRTUAL_HOST=mjpeg-stream-faker.dev.angelcam.com \
-e VIRTUAL_PORT=5000 --name=mjpeg-stream-faker mjpeg-stream-faker /srv/run.sh


# copy video to be streamed into the container
docker cp /tmp/video.mp4 mjpeg-stream-faker:/srv/mjpeg-stream-producer/
```

# Based on: 
## Video Streaming with Flask Example
https://github.com/log0/video\_streaming\_with\_flask\_example


### Website
http://www.chioka.in

### Description
Modified to support streaming out with webcams, and not just raw JPEGs.

### Credits
Most of the code credits to Miguel Grinberg, except that I made a small tweak. Thanks!
http://blog.miguelgrinberg.com/post/video-streaming-with-flask

### Usage
1. Install Python dependencies: cv2, flask. (wish that pip install works like a charm)
2. Run "python main.py".
3. Navigate the browser to the local webpage.