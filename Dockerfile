FROM ubuntu:trusty

RUN apt-get update && apt-get -qy install git python-pip python-opencv python-dev
RUN cd /srv && git clone https://kacerr@bitbucket.org/kacerr/mjpeg-stream-producer.git
RUN pip install flask numpy --upgrade

RUN echo '#!/usr/bin/env bash\n\
cd /srv/mjpeg-stream-producer/\n\
python main.py\n\
echo "sucessfully started, returning 0 to make docker happy\n'\
>> /srv/run.sh && chmod 700 /srv/run.sh