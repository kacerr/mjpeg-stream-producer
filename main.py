#!/usr/bin/env python
#
# Project: Video Streaming with Flask
# Author: Log0 <im [dot] ckieric [at] gmail [dot] com>
# Date: 2014/12/21
# Website: http://www.chioka.in/
# Description:
# Modified to support streaming out with webcams, and not just raw JPEGs.
# Most of the code credits to Miguel Grinberg, except that I made a small tweak. Thanks!
# Credits: http://blog.miguelgrinberg.com/post/video-streaming-with-flask
#
# Usage:
# 1. Install Python dependencies: cv2, flask. (wish that pip install works like a charm)
# 2. Run "python main.py".
# 3. Navigate the browser to the local webpage.
from flask import Flask, render_template, Response
from flask import request
from camera import VideoCamera
from datetime import datetime

#Statements

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')

def gen(camera, client_info):
    start=datetime.now()
    while True:
        frame = camera.get_frame(app, client_info)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        #print datetime.now()-start
        #print "frames read: " + str(camera.frames_read)
        #print "frames sent: " + str(camera.frames_sent)


@app.route('/video_feed')
def video_feed():
    client_info = request.environ['REMOTE_ADDR']
    app.logger.info ("serving: " + request.environ['REMOTE_ADDR'])
    return Response(gen(VideoCamera(), client_info),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, threaded=True)